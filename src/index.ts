import vanillaTilt from 'vanilla-tilt';
import { initToggle } from './scripts/toggle';

const MAX_RESULT_LENGTH = 20;
const BACKSPACE = 'Backspace';
const CLEAR = 'c';
const DIVIDE = '/';
const DOT = '.';
const EQUAL = 'Enter';
const MINUS = '-';
const MULTIPLY = '*';
const MODUULO = '%';
const PLUS = '+';
const PLUS_MINUS = '+/-';
const ZERO = '0';
const DOUBLE_ZEROS = '00';
const SIGNS = [DOT, DIVIDE, MINUS, MULTIPLY, MODUULO, PLUS, PLUS_MINUS];
const NUMERICALS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const NOT_AS_START = SIGNS.concat(DOUBLE_ZEROS, ZERO);
const NON_NUMERICALS = [BACKSPACE, CLEAR, EQUAL];
const VALID_INPUTS = NUMERICALS.concat(NON_NUMERICALS).concat(NOT_AS_START);
const calculatorContainer = document.querySelector<HTMLElement>('.calculator-container');
const result = document.querySelector('.result');
const pads = document.querySelectorAll('.num');

const updateResult = (char: string) => {
  if (!result) {
    return;
  }
  result.innerHTML += char;
};

const isLastCharsign = (expression: string) => {
  if (!expression) {
    return false;
  }
  return SIGNS.includes(expression[expression.length - 1]);
};

const init = () => {
  if (!calculatorContainer) {
    console.error('no calculator element found');
    return;
  }
  if (!result) {
    console.error('no calculator result element found');
    return;
  }

  vanillaTilt.init(calculatorContainer, {
    reverse: true,
    max: 5
  });

  let plusMinusSign = '';
  const handlePlusMinus = (input: string): string => {
    if (input === PLUS) {
      plusMinusSign = MINUS;
    } else {
      plusMinusSign = PLUS;
    }
    return plusMinusSign;
  };

  const handleInput = (input: string) => {
    if (!VALID_INPUTS.includes(input)) {
      return;
    }

    if (!input) {
      return;
    }

    if (!result.innerHTML && NOT_AS_START.includes(input)) {
      return;
    }

    if (input === CLEAR) {
      result.innerHTML = '';
      return;
    }

    if (input === BACKSPACE) {
      result.innerHTML = result.innerHTML.slice(0, result.innerHTML.length - 1);
      return;
    }

    if (input === EQUAL) {
      if (isLastCharsign(result.innerHTML)) {
        return;
      }
      result.innerHTML = eval(result.innerHTML);
      return;
    }

    if (result.innerHTML.length > MAX_RESULT_LENGTH) {
      return;
    }

    if (SIGNS.includes(input)) {
      if (!result.innerHTML) {
        return;
      }
      if (isLastCharsign(result.innerHTML)) {
        return;
      }
      if (input === PLUS_MINUS) {
        updateResult(handlePlusMinus(input));
        return;
      }

      if (!result.innerHTML.includes(input)) {
        updateResult(input);
        return;
      }

      return;
    }

    updateResult(input);
  };

  document.addEventListener('keydown', (event: KeyboardEvent) => {
    handleInput(event.key);
  });

  pads.forEach((p) => {
    p.addEventListener('click', () => {
      const v = p.getAttribute('data-value');
      if (!v) {
        return;
      }
      handleInput(v);
    });
  });

  initToggle();
};

init();
