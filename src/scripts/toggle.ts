const toggle = document.querySelector('.toggle');

const themeNames = ['glass', 'neumorphic'];

const toggleMode = () => {
  const body = document.querySelector('body');
  if (!body || !toggle) {
    return;
  }
  toggle.classList.toggle('active');
  body.classList.remove(...themeNames);
  if (toggle.classList.contains('active')) {
    body.classList.add('neumorphic');
  } else {
    body.classList.add('glass');
  }
  return;
};

export const initToggle = (): void => {
  toggle?.addEventListener('click', toggleMode);
};
